//
//  Analyzer.h
//  TP_lab6_1stTask
//
//  Created by Admin on 09.04.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Analyzer : NSObject

@property NSMutableDictionary *result;

-(NSDictionary*)analyze:(NSString*)inputString;
-(void)showResult;

@end
