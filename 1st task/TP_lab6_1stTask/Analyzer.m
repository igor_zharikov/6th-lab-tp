//
//  Analyzer.m
//  TP_lab6_1stTask
//
//  Created by Admin on 09.04.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "Analyzer.h"

@implementation Analyzer

@synthesize result = _result;

-(NSDictionary*)analyze:(NSString *)inputString{
    NSArray *words = [inputString componentsSeparatedByString:@" "];
    NSMutableDictionary *keeper = [NSMutableDictionary dictionaryWithCapacity:2];
    NSNumber *number;
    for(NSString *item in words){
        if ( (number = [keeper valueForKey:item]) == nil){
            [keeper setObject:[NSNumber numberWithInt:1] forKey:item];
        }
        else{
            [keeper setObject:[NSNumber numberWithLong:[number integerValue] + 1] forKey:item];
        }
    }
    NSArray *arraySortedByValue = [keeper keysSortedByValueUsingComparator:^(id obj1, id obj2){
        if ( [obj1 integerValue] > [obj2 integerValue]){
            return (NSComparisonResult)NSOrderedAscending;
        }
        if ( [obj1 integerValue] < [obj2 integerValue]){
            return (NSComparisonResult)NSOrderedDescending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    int count = 0;
    for(id key in arraySortedByValue){
        count++;
        [_result setObject:keeper[key] forKey:key];
        if ( count == 5){
            break;
        }
    }
    return _result;
}

-(void)showResult{
    NSLog(@"%@", _result);
}

-(id)init{
    self = [super init];
    if ( self){
        _result = [NSMutableDictionary dictionaryWithCapacity:5];
    }
    return self;
}

@end
