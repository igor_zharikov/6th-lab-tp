//
//  main.m
//  TP_lab6_1stTask
//
//  Created by Admin on 09.04.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Analyzer.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Analyzer *analyzer = [[Analyzer alloc] init];
        [analyzer analyze:@"123 123 sdc 34r34 fdgvdv 123 dfvmdfn dfvkdjk dvkdfjbk drkvdjgk 12 12 34 34"];
        [analyzer showResult];
    }
    return 0;
}
