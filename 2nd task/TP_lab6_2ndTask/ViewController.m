//
//  ViewController.m
//  TP_lab6_2ndTask
//
//  Created by Admin on 09.04.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController  ()
@property (weak, nonatomic) IBOutlet UILabel *weatherInfo;
@property (weak, nonatomic) IBOutlet UIButton *getWeatherButton;
@property (weak, nonatomic) IBOutlet UILabel *latitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *longitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *weatherLabel;

@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@end

@implementation ViewController{
     CLLocationManager *locationManager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    [locationManager requestWhenInUseAuthorization];
    //[locationManager startUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)refresh:(id)sender {
    [locationManager startMonitoringSignificantLocationChanges];
    [locationManager startUpdatingLocation];
}
-(NSArray*)getWeatherAndCity:(NSString *)latitude longitude:(NSString*) longitude{
    //NSLog(@"Enter in getWeather");
    NSString *weather;
    NSString *left = @"https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(SELECT%20woeid%20FROM%20geo.places%20WHERE%20text%3D%22(";
    NSString *right = @")%22)&format=json&diagnostics=true&callback=";
    NSString *urlString = [[[[left stringByAppendingString:latitude] stringByAppendingString:@","] stringByAppendingString:longitude] stringByAppendingString:right];
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    NSData *contents = [[NSData alloc] initWithContentsOfURL:url];
    NSDictionary *forCasting = [NSJSONSerialization JSONObjectWithData:contents options:NSJSONReadingMutableContainers error:nil];
    //NSLog(@"%@", forCasting);
    NSDictionary *results = [[forCasting valueForKey:@"query"] valueForKey:@"results"] ;
    NSDictionary *channel;
    if ( results != nil){
        channel = [results valueForKey:@"channel"];
    }
    NSString *city;
    if ( channel != nil){
        NSDictionary* item = [channel valueForKey:@"item"];
        NSString *temp = [[item valueForKey:@"condition"] valueForKey:@"temp"];
        int temperature = [temp intValue];
        double tempC = (temperature - 32) * 5 / 9.0;
        UIColor *color;
        if ( tempC < 0){
            color = [UIColor blueColor];
        }
        else{
            if ( tempC < 20){
                color = [UIColor greenColor];
            }
            else{
                color = [UIColor redColor];
            }
        }
        _weatherLabel.textColor = color;
        weather = [[NSString stringWithFormat:@"%.1lf", tempC] stringByAppendingString:@"C"];
        city = [[channel valueForKey:@"location"] valueForKey:@"city"];
        //NSLog(@"city: %@", city);
    }
    NSArray *result = [NSArray arrayWithObjects:weather,city, nil];
    return result;
}
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}
-(void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    CLLocation *newLocation = [locations lastObject];
    [locationManager stopUpdatingLocation];
    //[locationManager stopMonitoringSignificantLocationChanges];
    //NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    NSArray *cityAndWeather ;
    if (currentLocation != nil) {
        _longitudeLabel.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        _latitudeLabel.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        //NSLog(@"Before enter in getWeather");
        cityAndWeather = [self getWeatherAndCity:_latitudeLabel.text longitude: _longitudeLabel.text];
        _weatherLabel.text = [cityAndWeather objectAtIndex:0];
        
        _cityLabel.text = [cityAndWeather objectAtIndex:1];
    }
}

@end
